import tkinter as tk
import pygubu
from tkinter import *

from urllib.request import urlopen
from html.parser import HTMLParser
import pygal
import cairosvg
import cssselect
import tinycss
import lxml
from PIL import Image

 

 


class MyHTMLParser(HTMLParser):
    temperatures = {}
    humidity = {}
    printData = False
    printTable = False
    station = '' 
    def handle_starttag(self, tag, attrs):
        if tag == 'table':
            if attrs[0] == ('class','online'):
                self.printTable = True 
        if tag == 'tr':
            self.column = 0
        if tag == 'td':
            self.column += 1
            if self.printTable == True:
                self.printData = True
    def handle_endtag(self, tag):
        if tag == 'table':
            self.printTable = False
        if tag == 'td':
            self.printData = False

    def handle_data(self, data):
        if self.printData == True:
            if self.column == 1:
                self.station = data
            if self.column == 2:
                self.temperatures[self.station]=data
            if self.column == 3:
                self.humidity[self.station]=data
            
                


urlMeteo = urlopen('http://www.arso.gov.si/vreme/napovedi%20in%20podatki/vreme_avt.html')
htmlPage = urlMeteo.read().decode('UTF-8')
parser = MyHTMLParser()
parser.feed(str(htmlPage))


dict1 = parser.temperatures
dict2= parser.humidity


mesta = list(dict1.keys())
temperature = list(dict1.values())
vlaznost = list(dict2.values())


#za nekatere postaje ni podanih trenutnih podatkov; te postaje odstranimo
for t in temperature:
    if t == '-':
        i = temperature.index(t)
        del temperature[i]
        del mesta[i]
        del vlaznost[i]
for t in temperature:
    if t == '-':
        i = temperature.index(t)
        del temperature[i]
        del mesta[i]
        del vlaznost[i]
for j in range(len(temperature)):
    temperature[j] = int(temperature[j])
    vlaznost[j] = int(vlaznost[j])
          

#graf temperatur - nerazporejeno
bar_chart = pygal.Bar(x_label_rotation=90)                                            
bar_chart.add('Temperature', temperature)           
bar_chart.x_labels= mesta
bar_chart.render_to_png('temp-nerazporejeno.png')                                          
Image.open('temp-nerazporejeno.png').save('temp-nerazporejeno.gif')

#graf vlažnosti - nerazporejeno
bar_chart = pygal.Bar(x_label_rotation=90)                                            
bar_chart.add('Vlažnost', vlaznost)                                             
bar_chart.x_labels= mesta
bar_chart.render_to_png('vla-nerazporejeno.png')
Image.open('vla-nerazporejeno.png').save('vla-nerazporejeno.gif')
    
#temperature in vlažnost - nerazporejeno
bar_chart = pygal.Bar(x_label_rotation = 90)
bar_chart.title = 'Temperature in vlažnost'
bar_chart.x_labels= mesta
bar_chart.add('Temperature', temperature)
bar_chart.add('Vlažnost', vlaznost)
bar_chart.render_to_png('oboje-nerazporejeno.png')
Image.open('oboje-nerazporejeno.png').save('oboje-nerazporejeno.gif')


# mesta sortiramo po abecedi
mesta_sort = sorted(mesta)
    
#temperature - mesta v abecednem redu
s1 = sorted(list(zip(mesta, temperature)))
temp_abc = []
for x in s1:
    temp_abc.append(x[1])
bar_chart = pygal.Bar(x_label_rotation=90)
bar_chart.title = 'Temperature - mesta v abecednem redu'
bar_chart.add('Temperature', temp_abc)                                             
bar_chart.x_labels= mesta_sort
bar_chart.render_to_png('temp-abc.png')
Image.open('temp-abc.png').save('temp-abc.gif')
    
#vlažnost - mesta v abecednem redu
s2 = sorted(list(zip(mesta, vlaznost)))
vla_abc = []
for x in s2:
    vla_abc.append(x[1])
bar_chart = pygal.Bar(x_label_rotation=90)
bar_chart.title = 'Vlažnost - mesta v abecednem redu'
bar_chart.add('Vlažnost', vla_abc)                                             
bar_chart.x_labels= mesta_sort
bar_chart.render_to_png('vla-abc.png')
Image.open('vla-abc.png').save('vla-abc.gif')

#temperature in vlažnost - mesta v abecednem redu
bar_chart = pygal.Bar(x_label_rotation = 90)
bar_chart.title = 'Temperature in vlažnost'
bar_chart.x_labels= mesta_sort
bar_chart.add('Temperature', temp_abc)
bar_chart.add('Vlažnost', vla_abc)
bar_chart.render_to_png('oboje-abc.png')
Image.open('oboje-abc.png').save('oboje-abc.gif')



dict_krajsi={}
for ime, temp in dict1.items():
    if temp == '-':
        pass
    else:
        dict_krajsi[ime] = temp
    

#sortiramo še po temperaturi (naraščajoče)
s_temp = sorted(dict_krajsi.items(), key=lambda x: int(x[1]))     #slovar parov '(mesto, temperatura)' urejenih po naraščajočih temperaturah
mesta2 = []
for x in s_temp:
    mesta2.append(x[0])
temperature2 = []
for x in s_temp:
    temperature2.append(x[1])

#ponovimo kot pri slovarju dict1; odstraniti moramo postaje za katere ni podanih trenutnih podatkov

for j in range(len(temperature2)):
    temperature2[j] = int(temperature2[j])



#graf temperatur - naraščajoče
bar_chart = pygal.Bar(x_label_rotation=90)
bar_chart.title = 'Temperatura - naraščajoče'
bar_chart.add('Temperatura - naraščajoče', temperature2)                                             
bar_chart.x_labels= mesta2
bar_chart.render_to_png('temp-narasc.png')
Image.open('temp-narasc.png').save('temp-narasc.gif')

#prikaz obojega - sortirano po temperaturi
v = []
for c in mesta2:
    v.append(int(dict2[c]))
bar_chart = pygal.Bar(x_label_rotation = 90)
bar_chart.title = 'Temperature in vlažnost - sortirano po temperaturi'
bar_chart.x_labels= mesta2
bar_chart.add('Temperature', temperature2)
bar_chart.add('Vlažnost', v)
bar_chart.render_to_png('oboje-temp.png')
Image.open('oboje-temp.png').save('oboje-temp.gif')

#prikaz vlažnosti - sortirano po temperaturi
bar_chart = pygal.Bar(x_label_rotation = 90)
bar_chart.title = 'Vlažnost - sortirano po temperaturi'
bar_chart.x_labels= mesta2
bar_chart.add('Vlažnost', v)
bar_chart.render_to_png('vla-temp.png')
Image.open('vla-temp.png').save('vla-temp.gif')




s_vla = sorted(dict2.items(), key=lambda x: x[1])
mesta3 = []
for x in s_vla:
    mesta3.append(x[0])
vlaznost2 = []
for x in s_vla:
    vlaznost2.append(x[1])

#ponovimo kot pri slovarju dict1; odstraniti moramo postaje za katere ni podanih trenutnih podatkov
for t in vlaznost2:
    if t == '-':
        i = vlaznost2.index(t)
        del vlaznost2[i]
        del mesta3[i]
for t in vlaznost2:
    if t == '-':
        i = vlaznost2.index(t)
        del vlaznost2[i]
        del mesta3[i]
for t in vlaznost2:
    if t == '-':
        i = vlaznost2.index(t)
        del vlaznost2[i]
        del mesta3[i]
       
for j in range(len(vlaznost2)):
    vlaznost2[j] = int(vlaznost2[j])
   
#graf vlažnosti - naraščajoče
bar_chart = pygal.Bar(x_label_rotation=90)
bar_chart.title = 'Vlažnost - naraščajoče'
bar_chart.add('Vlažnost', vlaznost2)                                             
bar_chart.x_labels= mesta3
bar_chart.render_to_png('vla-narasc.png')
Image.open('vla-narasc.png').save('vla-narasc.gif')

#prikaz obojega - sortirano po vlažnosti
t = []
for x in mesta3:
    t.append(int(dict1[x]))

bar_chart = pygal.Bar(x_label_rotation=90)
bar_chart.title = 'Vlažnost in temperatura - sortirano po vlažnosti'
bar_chart.add('Vlažnost', vlaznost2)
bar_chart.add('Temperatura', t)  
bar_chart.x_labels= mesta3
bar_chart.render_to_png('oboje-vla.png')
Image.open('oboje-vla.png').save('oboje-vla.gif')

#prikaz temperature - sortirano po vlažnosti
bar_chart = pygal.Bar(x_label_rotation=90)
bar_chart.title = 'Temperatura - sortirano po vlažnosti'
bar_chart.add('Temperatura', t)                                             
bar_chart.x_labels= mesta3
bar_chart.render_to_png('temp-vla.png')
Image.open('temp-vla.png').save('temp-vla.gif')



class MyContainer(object):
    pass

class Application:
    def __init__(self, master):
        
        self.builder = builder = pygubu.Builder()
        builder.add_from_file('test.ui')

        self.mainwindow = builder.get_object('Frame_2', master)
        self.mainwindow.pack()
        self.RightFrame = builder.get_object('RightFrame',master)
        self.canvas = Canvas(self.RightFrame, width = 900, height = 700, bg='pink')

        self.container = MyContainer()
        builder.import_variables(self.container)
        
        self.container.sortiranje.trace("w", self.value_changed)
        self.container.temperatura_var.trace("w", self.value_changed)
        self.container.vlaznost_var.trace("w", self.value_changed)
                
        self.canvas.pack(expand = YES, fill = BOTH)

        self.gif1 = PhotoImage(file = 'oboje-nerazporejeno.gif')

        self.canvas.create_image(50, 50, image = self.gif1, anchor = NW)

          
    def value_changed(self, index, mode, a):
        self.update_graph(
                     self.container.sortiranje.get(),
                     self.container.temperatura_var.get(),
                     self.container.vlaznost_var.get())
        
    def update_graph(self,sortiranje, temperatura, vlaznost):
        if temperatura == 0 and vlaznost == 0:
            self.gif = PhotoImage(file = 'oboje-nerazporejeno.gif')
            
        if temperatura == 1 and vlaznost == 1:
            if sortiranje == 1:
                self.gif = PhotoImage(file = 'oboje-temp.gif')
            if sortiranje == 2:
                self.gif = PhotoImage(file = 'oboje-vla.gif')
            if sortiranje == 3:
                self.gif = PhotoImage(file = 'oboje-abc.gif')
            if sortiranje == 4:
                self.gif = PhotoImage(file = 'oboje-nerazporejeno.gif')
        
        if temperatura == 1 and vlaznost == 0:
            if sortiranje == 1:
                self.gif = PhotoImage(file = 'temp-narasc.gif')
            if sortiranje == 2:
                self.gif = PhotoImage(file = 'temp-vla.gif')
            if sortiranje == 3:
                self.gif = PhotoImage(file = 'temp-abc.gif')
            if sortiranje == 4:
                self.gif = PhotoImage(file = 'temp-nerazporejeno.gif')
                
        if temperatura == 0 and vlaznost == 1:
            if sortiranje == 1:
                self.gif = PhotoImage(file = 'vla-temp.gif')
            if sortiranje == 2:
                self.gif = PhotoImage(file = 'vla-narasc.gif')
            if sortiranje == 3:
                self.gif = PhotoImage(file = 'vla-abc.gif')
            if sortiranje == 4:
                self.gif = PhotoImage(file = 'vla-nerazporejeno.gif')
                
        self.canvas.create_image(50, 50, image = self.gif, anchor = NW)
            
       
    


if __name__ == '__main__':
    root = tk.Tk()
    app = Application(root)
    root.mainloop()

  


 
